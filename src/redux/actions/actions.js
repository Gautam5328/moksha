import { ActionTypes } from "./actionTypes";

export const successMessage=(message)=>{
    return{
        type : ActionTypes.SUCCESS,
        payload : message
    }
}

export const failureMessage=(message)=>{
    return{
        type : ActionTypes.FAILURE,
        payload : message
    }
}


export const setUsername=(username)=>{
    return{
        type : ActionTypes.SETUSERNAME,
        payload : username
    }
}



export const setFirstname=(firstname)=>{
    return{
        type : ActionTypes.SETFIRSTNAME,
        payload : firstname
    }
}


export const setLastname=(lastname)=>{
    return{
        type : ActionTypes.SETLASTNAME,
        payload : lastname
    }
}


export const setUserData=(data)=>{
    return{
        type : ActionTypes.SETUSERDATA,
        payload : data
    }
}
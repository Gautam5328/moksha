import { ActionTypes } from "../actions/actionTypes";

const initialState={
    username : "",
    firstname : "",
    lastname : "",
    userData : []
}

export const setDataReducer=(state=initialState,{type,payload})=>{
    switch(type){
        
        case ActionTypes.SETUSERDATA :
            return{
                ...state,
                userData : payload
            }

        case ActionTypes.SETFIRSTNAME :
            return{
                ...state,
                firstname : payload
            }

        case ActionTypes.SETUSERNAME :
            return{
                ...state,
                username : payload
            }

            
        case ActionTypes.SETLASTNAME :
            return{
                ...state,
                lastname : payload
            }

        default:
            return state;

    }   
}
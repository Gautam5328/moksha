import { ActionTypes } from "../actions/actionTypes";

const initialState={
    successMessage : "Success",
    failureMessage : "Failure",
}

export const setMessageReducer=(state=initialState,{type,payload})=>{
    switch(type){
        case ActionTypes.SUCCESS:
            return {
                ...state,
                successMessage : payload
            }

        case ActionTypes.FAILURE:
            return {
                ...state,
                failureMessage : payload
            }

        default:
            return state;

    }   
}
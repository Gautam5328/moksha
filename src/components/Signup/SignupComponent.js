import React from 'react'
import {Form,Button,Row,Col} from "react-bootstrap";
import "./SignupComponent.css";
import validator from 'validator';
import { connect } from 'react-redux';
import {setFirstname} from "../../redux/actions/actions"
import axios from "axios";
import {Link} from "react-router-dom";


class SignupComponent extends React.Component {
    
    constructor(props){
        super(props);
        this.state={
            isdisabled : true,
            passwordStrength : "",
            matchPassword : "",
            firstname : "",
            lastname : "",
            email : "",
            username: "",
            password : "",
            confirmpassword : "",
            errors: {    
                   firstnameLength : "",
                   firstnameChar : "",
                   lastnameLength : "",
                   lastnameChar : "",
                   email : "",
                   username: "",
                   password : "",
                   confirmpassword : "",
                   matchPasswordValue : "",
              }            
        }

        this.onSubmit=this.onSubmit.bind(this);
    }


        
    onSubmit(e){
        e.preventDefault();
        axios.post(`https://6108c452d73c6400170d3a3d.mockapi.io/MokshaUserData`,{
            firstname : this.state.firstname,
            lastname : this.state.lastname,
            email : this.state.email,
            username : this.state.username,
            password : this.state.password,
            confirmpassword : this.state.confirmpassword
        }).then(()=>{
           // this.props.history.push('/login')
            console.log(this.state);

        })

        const {dispatch}=this.props;
        dispatch(setFirstname(this.state.firstname))
    }

    checkPasswordStrength=(event)=>{
        event.preventDefault();
        const {value}=event.target
        if(validator.isStrongPassword(value,{
            minLength : 8,minLowercase:1,
            minUppercase:1,minNumbers:1,minSymbols:1
        })){
            this.setState({passwordStrength:"Strong Password"})
        }
        else{
            this.setState({passwordStrength:"Weak Password"})
        }
    }
    
    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;

        

        if(!this.state.errors){
            this.state.isdisabled=false;
        }
       
        const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
        const validNameRegex = RegExp('^[a-zA-Z]+$');
        switch (name) {
            
          case 'firstname': 
            errors.firstnameLength = 
              value.length < 3
                ? 'First Name must be atleast 3 characters long!'
                : '' 
            errors.firstnameChar =  validNameRegex.test(value)
                ? ''
                : 'Only Characters are allowed'  ;
            break;
           case 'lastname' :
               errors.lastnameLength=
               value.length < 3
               ? 'Last Name must be atleast 3 characters long!' : '' 
                errors.lastnameChar =  validNameRegex.test(value)
                ? ''
               : 'Only Characters are allowed'  ;
               break; 
          case 'email': 
            errors.email = 
              validEmailRegex.test(value)
                ? ''
                : 'Email is not valid!';
            break;
          case 'username' :
              errors.username = 
              value.length<5
              ?  'Username should be atleast 5 characters long' : '';
              break;  
          case 'password':
            this.state.matchPassword=value;
            errors.password = 
              value.length < 8
                ? 'Password must be atleast 8 characters long!'
                : '';
            break;

            
          case 'confirmpassword':
            errors.confirmpassword = 
              value.length < 8
                ? 'Password must be atleast 8 characters long!'
                : '';
                if(value===this.state.matchPassword){
                    errors.matchPasswordValue=" "
                }
                else{
                    errors.matchPasswordValue="Password's don't match "
                }
            break;
  
          default:
            break;
        }
      
        this.setState({errors, [name]: value}, ()=> {
            
    
        })
      }




    render(){
        
        const {errors} = this.state;

        return (
            <div className="jumbotron">
              <Row>    
              <Col  md={{span : 3}} xs={{ span: 6 }} sm={{ span: 4 }} xl={{ span: 4 }}>                
                <Form.Group>
                    <Form.Label className="labels">First Name</Form.Label>
                    <Form.Control
                        autoFocus
                        type="text"
                        placeholder="First Name"
                        name="firstname"
                        value={this.state.firstname}
                        onChange={(e)=>this.setState({firstname : e.target.value})}
                        onChange={this.handleChange}
                    />
                    
                        {errors.firstnameLength.length > 0 && 
                        <span className='error'>{errors.firstnameLength}</span>}
                         {errors.firstnameChar.length > 0 && 
                        <span className='error'>{errors.firstnameChar}</span>}


                </Form.Group>
   
                
                <Form.Group>
                    <Form.Label className="labels">Last Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Last Name"
                        name="lastname"
                        value={this.state.lastname}
                        onChange={(e)=>this.setState({lastname:e.target.value})}
                        onChange={this.handleChange}
                    />


                     {errors.lastnameLength.length > 0 && 
                        <span className='error'>{errors.lastnameLength}</span>}
                    {errors.lastnameChar.length > 0 && 
                        <span className='error'>{errors.lastnameChar}</span>}

                </Form.Group>
            
    
                <Form.Group>
                    <Form.Label className="labels">Email ID</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Email ID"
                        name="email"
                        value={this.state.email}
                        onChange={(e)=>this.setState({email:e.target.value})}
                        onChange={this.handleChange}
                    />
                    {errors.email.length > 0 && 
                        <span className='error'>{errors.email}</span>}

                </Form.Group>
            </Col>            
    
            <Col  md={{span : 3}} xs={{ span: 6 }} sm={{ span: 4 }} xl={{ span: 4 }}>                
                <Form.Group>
                    <Form.Label className="labels">Username</Form.Label>
                    <Form.Control                        
                        type="text"
                        placeholder="UserName"
                        name="username"
                        value={this.state.username}
                        onChange={(e)=>this.setState({username:e.target.value})}
                        onChange={this.handleChange}
                    />
                    {errors.username.length > 0 && 
                        <span className='error'>{errors.username}</span>}

                </Form.Group>

                
                <Form.Group>
                    <Form.Label className="labels">Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        name="password"
                        value={this.state.password}
                        onChange={(e)=>this.setState({password:e.target.value})}
                        onChange={this.handleChange}
                    />
                    {errors.password.length > 0 && 
                        <span className='error'>{errors.password}</span>}
                    {this.state.passwordStrength}


                </Form.Group>

                
                <Form.Group>
                    <Form.Label className="labels">Confirm Password</Form.Label>
                    <Form.Control
                        
                        type="password"
                        placeholder="Confirm Password"
                        name="confirmpassword"
                        value={this.state.confirmpassword}
                        onChange={(e)=>this.setState({confirmpassword:e.target.value})}
                        onChange={this.handleChange}
                    />
                    {errors.confirmpassword.length > 0 && 
                        <span className='error'>{errors.confirmpassword}</span>}
                      
                    {errors.matchPasswordValue.length > 0 && 
                        <span className='error'>{errors.matchPasswordValue}</span>}  

                </Form.Group>
            </Col>
            </Row>           
                <Button onClick={this.onSubmit}  /* disabled={this.state.isdisabled} */>Submit</Button> 
            </div>
   
             )
        }

    }

export default connect()(SignupComponent)

import React, { Component } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import "./LoginComponent.css"
import {connect} from "react-redux";
import Auth from "../Authentication/Auth";

import {setUsername,setFirstname,setLastname,setUserData,successMessage,failureMessage} from "../../redux/actions/actions"

import axios from "axios";
import {withRouter} from 'react-router';


class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      userData : [],
      errors: {
        usernameEmpty: "",
        passwordEmpty: "",
      },
    };
  }

  componentDidMount(){
  
      const {dispatch}=this.props
      axios.get(`https://6108c452d73c6400170d3a3d.mockapi.io/MokshaUserData`)
      .then((res)=>{
          console.log(res.data);
          this.setState({userData : res.data});
          dispatch(setUserData(res.data));
      })
  }

  handleClick=(e)=>{
    
      e.preventDefault();
    
      const {dispatch}=this.props;
      this.state.userData.map((data)=>{        
          if(data.username==this.state.username && data.password==this.state.password){
            dispatch(successMessage("Login Success"))
            dispatch(setUsername(this.state.username));
            dispatch(setFirstname(data.firstname));
            dispatch(setLastname(data.lastname));
            Auth.authenticate();
            this.props.history.push('/home')
          }
          else
          {
            dispatch(failureMessage("Login Failure")) 
          }
      })
      
      
    
  }


  handleChange = (event) => { 
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;

    switch (name) {
      case "username":
        errors.usernameEmpty =
          value.length < 1 ? "Username cannot be Empty" : "";
        break;

      case "password":
        errors.passwordEmpty =
          value.length < 1 ? "Password cannot be Empty" : "";
        break;

      default:
        break;
    }

    this.setState({ errors, [name]: value }, () => {});
  };

  render() {
    const { errors } = this.state;

    return (
      <div className="LoginComponent">
        <div className="jumbotron">
          <Row>
            <Col
              md={{ span: 3 }}
              xs={{ span: 6 }}
              sm={{ span: 4 }}
              xl={{ span: 4 }}
            >
              <Form.Group>
                <Form.Label className="labels">Username</Form.Label>
                <Form.Control
                  autoFocus
                  type="text"
                  placeholder="Username"
                  name="username"
                  onChange={(e) => this.setState({ username: e.target.value })}
                  onChange={this.handleChange}
                />

                <span className='error'>{errors.usernameEmpty}</span>
              </Form.Group>
            </Col>
          </Row>

          <Row>
            <Col
              md={{ span: 3 }}
              xs={{ span: 6 }}
              sm={{ span: 4 }}
              xl={{ span: 4 }}
            >
              <Form.Group>
                <Form.Label className="labels">Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  name="password"
                  onChange={(e) => this.setState({ password: e.target.value })}
                  onChange={this.handleChange}
                />

                <span className='error'>{errors.passwordEmpty}</span>
              </Form.Group>
            </Col>
          </Row>

          <Button className="primary" onClick={this.handleClick}>Login</Button>
        </div>
      </div>
    );
  } 
}




export default withRouter(connect()(LoginComponent));

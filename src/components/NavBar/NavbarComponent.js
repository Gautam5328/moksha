import React,{useState} from 'react'
import { Navbar,Nav,Container } from 'react-bootstrap';
import { BrowserRouter as Router ,Route,Switch,Link } from "react-router-dom";
import "./NavbarComponent.css"

function NavbarComponent() {
  return (
    <div className="NavbarComponent">
      <Navbar bg="primary" variant="dark">
        <Container>
            <Link to="/">
              <Navbar.Brand id="headers_navigation">Moksha</Navbar.Brand>
            </Link>
            <Nav className="ml-auto">
              <Link to="/home">
                <p id="headers_navigation">Home</p>
              </Link>
              <Link to="/signup">
                <p id="headers_navigation">SignUp</p>
              </Link>
              <Link to="/login">
                <p id="headers_navigation">Login</p>
              </Link>
            </Nav>
        </Container>
      </Navbar>
    </div>
  );
}

export default NavbarComponent;

import React,{useState,useEffect} from 'react'
import {Button, Form} from "react-bootstrap";
import "./Home.css"
import {useSelector,useDispatch} from "react-redux";
import Auth from '../Authentication/Auth';
import NavbarComponent from '../NavBar/NavbarComponent';
import { useHistory } from "react-router-dom"


function Home() { 

  
  let history = useHistory()

  function logout(){
  
    console.log("Logout123")
    Auth.signout();
    history.push("/login")
  }

    const username=useSelector((state)=>state.finalreducers.username);
    const firstname=useSelector((state)=>state.finalreducers.firstname);
    const lastname=useSelector((state)=>state.finalreducers.lastname);

    return (
        <div>          
          <NavbarComponent/>
          <h1>Home</h1>
          <h2>Welcome {firstname} {lastname}</h2>
          <Button onClick={logout}>Logout</Button>
        </div>
    )
}

export default Home

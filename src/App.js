import React from "react";
import {BrowserRouter as Router,Route,Link,Switch} from "react-router-dom";
import Home from "./components/Home/Home";
import NavbarComponent from "./components/NavBar/NavbarComponent";
import LoginComponent from "./components/Login/LoginComponent";
import SignupComponent from "./components/Signup/SignupComponent";
import PrivateRoute from "./components/Authentication/PrivateRoute";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          
          <PrivateRoute path="/home" component={Home} />       

          <Route path="/login">
            <NavbarComponent/>
            <LoginComponent/>
          </Route>

          <Route path="/signup">
            <NavbarComponent/>
            <SignupComponent/>
          </Route>

          <PrivateRoute path="/" component={Home}/>
          


        </Switch>
      </Router>
      
    </div>
  );
}

export default App;
